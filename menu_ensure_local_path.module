<?php
/**
 * Module file for the Menu Ensure Local Path module.
 *
 */

define('MENU_ENSURE_LOCAL_PATH_OPTIONS_VARIABLE', 'menu_ensure_local_path_menu_options');

/**
 * Implements hook_menu_link_alter().
 *
 * Prevent saving of menu links with absolute URLs including the domain name.
 */
function menu_ensure_local_path_menu_link_alter(&$item) {
  if (empty($item['op'])) {
    // No editing context.
    return;
  }
  if ($item['op'] != $item['submit']) {
    // Probably deleting the item.
    return;
  }

  // Get the options and see if we should act.
  $options = menu_ensure_local_path_get_menu_options($item['menu_name']);
  if (!$options['enabled'] || ($options['allow_override'] && !empty($item['menu_ensure_local_path_override']))) {
    return;
  }

  $original_url = trim($item['link_path']);
  $relative_url = menu_ensure_local_path_process_url($original_url);
  if ($relative_url == $original_url) {
    return;
  }

  // Update the menu item.
  $item['link_path'] = $relative_url;

  // Inform the user about the change.
  if ($options['show_message']) {
    drupal_set_message(t('The path has been changed to be relative to the current domain: <em>%relative_url</em>', array(
      '%relative_url' => $relative_url,
    )));
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function menu_ensure_local_path_form_menu_edit_menu_alter(&$form, &$form_state) {
  $original_url = url('user', array('absolute' => TRUE));
  $relative_url = menu_ensure_local_path_process_url($original_url);
  $options = menu_ensure_local_path_get_menu_options($form['old_name']['#value']);
  $form['ensure_local_path'] = array(
    '#type' => 'fieldset',
    '#title' => t('Local path options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['ensure_local_path']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Ensure local paths for new or updated items in this menu.'),
    '#description' => t('Check this to turn %absolute_url into %relative_url when saving a menu link', array(
      '%absolute_url' => $original_url,
      '%relative_url' => $relative_url,
    )),
    '#default_value' => $options['enabled'],
  );
  $form['ensure_local_path']['allow_override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow override'),
    '#description' => t('Check this to allow editors to override the path check and force their version.'),
    '#states' => array(
      'visible' => array(
        ':input[name="ensure_local_path[enabled]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => $options['allow_override'],
  );
  $form['ensure_local_path']['show_message'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show message'),
    '#description' => t('Check this to show a message about the path change to the editor.'),
    '#states' => array(
      'visible' => array(
        ':input[name="ensure_local_path[enabled]"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => $options['show_message'],
  );
  $form['#submit'][] = 'menu_ensure_local_path_form_menu_edit_menu_submit';
}

/**
 * Custom submit handler for the menu edit form.
 */
function menu_ensure_local_path_form_menu_edit_menu_submit(&$form, &$form_state) {
  $options = $form_state['values']['ensure_local_path'];
  $menu_name = $form_state['values']['menu_name'];
  menu_ensure_local_path_set_menu_options($menu_name, $options);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function menu_ensure_local_path_form_menu_edit_item_alter(&$form, &$form_state) {
  if ($form['link_path']['#type'] == 'value') {
    // Fixed value, nothing to do.
    return;
  }

  $menu_name = $form['original_item']['#value']['menu_name'];
  $options = menu_ensure_local_path_get_menu_options($menu_name);
  if (!$options['enabled'] || !$options['allow_override']) {
    return;
  }

  $form['link_path']['#ajax'] = array(
    'callback' => 'menu_ensure_local_path_link_path_callback',
    'wrapper' => 'menu-ensure-local-path-override',
    'method' => 'replace',
    'effect' => 'fade',
  );

  $description = '';
  $needs_override = FALSE;
  if (isset($form_state['values']['link_path'])) {
    $original_url = $form_state['values']['link_path'];
    $relative_url = menu_ensure_local_path_process_url($original_url);
    if ($relative_url != $original_url) {
      $needs_override = TRUE;
      $description = t('Without this option the entered path will be transformed to %relative_url', array(
        '%relative_url' => $relative_url,
      ));
    }
  }

  if ($needs_override) {
    $elements = array(
      'menu_ensure_local_path_override' => array(
        '#type' => 'checkbox',
        '#title' => t('Force absolute path'),
        '#default_value' => FALSE,
        '#prefix' => '<div id="menu-ensure-local-path-override">',
        '#suffix' => '</div>',
        '#description' => $description,
      ),
    );
  }
  else {
    $elements = array(
      'menu_ensure_local_path_override' => array(
        '#type' => 'hidden',
        '#default_value' => FALSE,
        '#prefix' => '<div id="menu-ensure-local-path-override">',
        '#suffix' => '</div>',
      ),
    );
  }
  $pos = array_search('link_path', array_keys($form)) + 1;
  $form = array_slice($form, 0, $pos, TRUE) + $elements + array_slice($form, $pos, count($form) - 1, TRUE);
}

/**
 * Ajax callback for instant feedback during editing of the link path.
 */
function menu_ensure_local_path_link_path_callback($form, $form_state) {
  return $form['menu_ensure_local_path_override'];
}

/**
 * Default menu options.
 *
 * @return array
 */
function menu_ensure_local_path_get_default_menu_options() {
  return array(
    'enabled' => FALSE,
    'allow_override' => FALSE,
    'show_message' => FALSE,
  );
}

/**
 * Retrieve options for the given menu name.
 *
 * @param string $menu_name
 * @return array
 * @see menu_ensure_local_path_get_default_menu_options().
 */
function menu_ensure_local_path_get_menu_options($menu_name) {
  $menu_options = variable_get(MENU_ENSURE_LOCAL_PATH_OPTIONS_VARIABLE, array());
  $default = menu_ensure_local_path_get_default_menu_options();
  if (!isset($menu_options[$menu_name])) {
    return $default;
  }
  return $menu_options[$menu_name] + $default;
}

/**
 * Store options for the given menu name.
 *
 * @param string $menu_name
 * @param array $options
 * @return array
 * @see menu_ensure_local_path_get_default_menu_options().
 */
function menu_ensure_local_path_set_menu_options($menu_name, $options) {
  $menu_options = variable_get(MENU_ENSURE_LOCAL_PATH_OPTIONS_VARIABLE, array());
  $default = menu_ensure_local_path_get_default_menu_options();
  $menu_options[$menu_name] = $options + $default;
  variable_set(MENU_ENSURE_LOCAL_PATH_OPTIONS_VARIABLE, $menu_options);
}

/**
 * Process the given URL.
 *
 * Check if a modification is needed, if so, apply  it.
 *
 * @param string $original_url
 * @return string
 */
function menu_ensure_local_path_process_url($original_url) {
  // Parse the URL.
  $parsed_url = parse_url($original_url);
  if (!$parsed_url) {
    // Parsing failed so we should definitely not touch this.
    return $original_url;
  }
  $scheme = $parsed_url['scheme'];
  $host = $parsed_url['host'];

  // Check if an action is required.
  if (!in_array(strtolower($scheme), array('http', 'https')) || strtolower($host) !== strtolower($_SERVER['HTTP_HOST'])) {
    // No absolute URL, non-HTTP protocol or link to an external domain.
    return $original_url;
  }

  // Remove the protocol and domain name and update the menu links link_path.
  $relative_url = str_replace($parsed_url['scheme'] . '://' . $parsed_url['host'], '', $original_url);
  return trim($relative_url, '/');
}